<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $surprise = isset($_POST["surprise"]) ? $_POST["surprise"] : "off";

    $name = $_POST['name'];
    if ($surprise == "on") {
        $query = ":" . str_rot13($name);
    } else {
        $query = $name;
    }

    header("Location: /" . $query);
    exit;
}
$query = empty($_GET["name"]) ? "Machin" : $_GET["name"];

if (substr($query, 0, 1) == ":") {
    $name = str_rot13(substr($query, 1));
} else {
    $name = $query;
}

?>
<!doctype html>
<html lang="en">
  <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Ta Gueule Culture</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
        <style>
         .vertical-center {
             min-height: 100%; /* Fallback for browsers do NOT support vh unit */
             min-height: 100vh;

             display: flex;
             align-items: center;
         }
         p.tg:first-letter {
             text-transform:capitalize;
             font-weight: bold;
         }
         p.tg:first-letter {
             font-weight: bold;
         }
        </style>
            <script>
             $( document ).ready(function() {
                 $("#explication").hide();
             });
             $("html").mousemove(function( event ) {
                 $("#explication").slideDown(1000);
             });
            </script>
  </head>

  <body>
      <section id="explication" class="jumbotron text-center">
          <div class="container">
              <h1 class="jumbotron-heading">Ta Gueule Culture</h1>
              <p class="lead text-muted">Donnez clairement le fond de votre pensée à qui vous savez.</p>
              <form method="post">
                  <div class="form-group row">
                      <div class="col-sm-4"><p class="float-right"> Et vous, à qui pensez-vous ? </p></div>
                      <div class="col-sm-4"><input class="form-control" type="text" name="name"></div>
                      <div class="col-sm-2"><button type="submit" class="float-left btn btn-primary">Sa Gueule</button></div>
                      <div class="col-sm-2 custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="surprise" name="surprise">
                          <label class="custom-control-label" for="surprise">Mode surprise</label>
                      </div>
                  </div>
              </form>
          </div>
      </section>

      <section class="vertical-center">
          <div class="container">
              <div class="h1 text-center">
                  <p class="tg">Ta</p>
                        <p class="tg">Gueule</p>
                              <p class="tg"><?php
                                echo(htmlspecialchars($name));
                                ?>
                  </p>
              </div>
          </div>
      </section>
  </body>
</html>
