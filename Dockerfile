FROM alpine as build
COPY build.sh .
WORKDIR /app
RUN /build.sh

FROM docker.io/nginx as runtime
COPY --from=build /app /usr/share/nginx/html
COPY index.html /usr/share/nginx/html

